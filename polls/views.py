from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, redirect
from django.utils import timezone
from django.urls import reverse
from django.views import generic

from .models import Choice, Question
from .forms import QuestionForm


class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return Question.objects.order_by('-pub_date')[:100]


class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'


class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'


def new_question(request):
    if request.method == "POST":
        question_form = QuestionForm(request.POST)
    
        if question_form.is_valid():
            q = Question(question_text=question_form.cleaned_data['question_text'], pub_date=timezone.now())
            q.save()
            q.choice_set.create(choice_text=question_form.cleaned_data['first_choice'], votes=0)
            q.choice_set.create(choice_text=question_form.cleaned_data['second_choice'], votes=0)
            q.choice_set.create(choice_text=question_form.cleaned_data['third_choice'], votes=0)
            
            return redirect(reverse('polls:index'))
    else:
        question_form = QuestionForm() 
        return render(request, 'polls/new-question.html', {'question_form': question_form,})

def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'polls/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))
