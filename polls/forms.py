from django.forms import Form, CharField
from .models import Question, Choice


class QuestionForm(Form):
    question_text =  CharField(label="question text", max_length=200)
    first_choice = CharField(label="First choice", max_length=200)
    second_choice = CharField(label="Second choice", max_length=200)
    third_choice = CharField(label="Third choice", max_length=200)
    
