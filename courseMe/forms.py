from django import forms
from .models import Course, Section, Lesson


class CourseForm(forms.ModelForm):
    class Meta:
        model = Course
        fields = ("name", "description", "image", "students", "is_public" )
        widgets = {
            'name' : forms.TextInput(attrs={"class": "prompt srch_explore", 'placeholder': 'Enter course name'}),
            'description' : forms.Textarea(attrs={'rows' : 3, 'placeholder' : 'Enter course description'}),
        }


class SectionForm(forms.ModelForm):
    class Meta:
        model = Section
        exclude = ('course',)

    def __init__(self, *args, **kwargs):
        super(SectionForm, self).__init__(*args, **kwargs)
        for field in self.fields.values(): 
            if isinstance(field, (forms.CharField)):
                field.widget.attrs['class'] = 'form-control'


class LessonForm(forms.ModelForm):
    class Meta:
        model = Lesson
        fields = '__all__'
        widgets = {
            'video': forms.FileInput(attrs={'accept': 'video/*'}),
        }

    def __init__(self, course_id=None, *args, **kwargs):
        super(LessonForm, self).__init__(*args, **kwargs)
        for field in self.fields.values(): 
            if isinstance(field, (forms.CharField)):
                field.widget.attrs['class'] = 'form-control'
        if course_id:
            self.fields["section"] = forms.ModelChoiceField(Section.objects.filter(course=course_id))
