# Generated by Django 3.1.2 on 2020-10-09 12:33

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('courseMe', '0023_auto_20201008_2040'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='is_public',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='lesson',
            name='can_see',
            field=models.ManyToManyField(blank=True, related_name='lesson_can_see', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='lesson',
            name='is_public',
            field=models.BooleanField(default=True),
        ),
    ]
