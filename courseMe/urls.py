from django.urls import path, include
from django.contrib.auth.decorators import login_required
from django.conf.urls.static import static
from . import views

app_name = 'courseMe'

urlpatterns = [
    path('', login_required(views.HomeView.as_view()), name='home'),
    path('create_new_course', login_required(views.NewCourseView.as_view()), name='create_new_course'),
    path('delete_course/<pk>/', views.DeleteCourseView.as_view(), name='delete_course'),
    path('create_new_section/<int:course_id>/', views.NewSectionView.as_view(), name='create_new_section'),
    path('create_new_lesson/<int:course_id>/', views.NewLessonView.as_view(), name='create_new_lesson'),
    path('update_lesson/<pk>/', views.UpdateLessonView.as_view(), name='update_lesson'),
    path('update_section/<pk>/', views.UpdateSectionView.as_view(), name='update_section'),
    path('delete_lesson/<pk>/', views.DeleteLessonView.as_view(), name='delete_lesson'),
    path('course_detail/<int:pk>/', login_required(views.CourseDetailView.as_view()), name='course_detail')
    
] 
