from django.apps import AppConfig


class CoursemeConfig(AppConfig):
    name = 'courseMe'
