from django.contrib import admin

from .models import Course, Section, Lesson

class SectionInLine(admin.TabularInline):
    model = Section

class CourseAdmin(admin.ModelAdmin):
    model = Course
    inlines = [SectionInLine]
    
class LessonAdmin(admin.ModelAdmin):
    model = Lesson

class SectionAdmin(admin.ModelAdmin):
    model = Section


admin.site.register(Course, CourseAdmin)
admin.site.register(Section, SectionAdmin)
admin.site.register(Lesson, LessonAdmin)

