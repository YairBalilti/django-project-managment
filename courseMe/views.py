from django.views import generic
from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
from django.contrib.auth.models import User
from django.db.models import Q
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.urls import reverse_lazy
from django import forms
from django.contrib.auth.mixins import PermissionRequiredMixin

from .models import Course, Section, Lesson
from .forms import CourseForm, SectionForm, LessonForm


class HomeView(generic.ListView):
    model = Course
    template_name = "courseMe/dashboard.html"
    context_object_name = "courses"

    def get_queryset(self):
        return Course.objects.filter(Q(owner=self.request.user) | Q(is_public=True) | Q(students__id=self.request.user.id))


class CourseDetailView(PermissionRequiredMixin, generic.DetailView):
    model = Course
    template_name = 'courseMe/course_detail.html'
    context_object_name = "course"

    def has_permission(self):
        course = self.get_object()
        return (self.request.user == course.owner or course.is_public or self.request.user in course.students.all())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        course = self.get_object()
        context["section_form"] = SectionForm(prefix="section")
        context["lesson_form"] = LessonForm(
            prefix="lesson", course_id=course.id)
        context["total_lessons"] = sum(
            len(section.lesson.all()) for section in course.section.all())
        return context


class NewCourseView(CreateView):
    template_name = "courseMe/‏‏create_new_course.html"
    form_class = CourseForm

    def get_success_url(self):
        return self.request.META.get('HTTP_REFERER')

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super(NewCourseView, self).form_valid(form)


class NewLessonView(CreateView):
    form_class = LessonForm
    prefix = "lesson"

    def get_success_url(self):
        return self.request.META.get('HTTP_REFERER')

    def form_valid(self, form):
        course_id = self.kwargs["course_id"]
        course_owner = Course.objects.get(pk=course_id)
        form.instance.course_owner = course_owner
        return super(NewLessonView, self).form_valid(form)


class NewSectionView(CreateView):
    form_class = SectionForm
    prefix = "section"
    template_name = 'courseMe/dashboard.html'

    def get_success_url(self):
        return reverse('courseMe:course_detail', args=[self.kwargs["course_id"]])

    def form_valid(self, form):
        course_id = self.kwargs["course_id"]
        course = Course.objects.get(pk=course_id)
        form.instance.course = course
        return super(NewSectionView, self).form_valid(form)


class UpdateLessonView(UpdateView):
    model = Lesson
    form_class = LessonForm

    def get_success_url(self):
        return self.request.META.get('HTTP_REFERER')


class UpdateSectionView(UpdateView):
    model = Section
    form_class = SectionForm

    def get_success_url(self):
        return self.request.META.get('HTTP_REFERER')


class DeleteCourseView(DeleteView):
    model = Course
    success_url = reverse_lazy('courseMe:home')

    def delete(self, request, *args, **kwargs):
        if self.get_object().owner == request.user:
            return super(DeleteCourseView, self).delete(request, *args, **kwargs)


class DeleteLessonView(DeleteView):
    model = Lesson

    def get_success_url(self):
        return self.request.META.get('HTTP_REFERER')

    def delete(self, request, *args, **kwargs):
        if self.get_object().section.course.owner == request.user:
            return super(DeleteLessonView, self).delete(request, *args, **kwargs)
