from django.db import models
from django.core.validators import FileExtensionValidator
from django.contrib.auth.models import User


def lesson_directory_path(instance, filename):
    return 'courses_videos/{0}/{1}-{2}'.format(instance.course_owner, instance.name, filename)

class Course(models.Model):
    name = models.CharField(max_length=128)
    description = models.CharField(max_length=256)
    owner = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name='my_own_courses', null=True)
    students = models.ManyToManyField(User, related_name='my_courses', blank=True)
    image = models.ImageField(default='courses_images/add_img.jpg', upload_to='courses_images')
    create_date = models.DateTimeField(auto_now=True)
    last_updated = models.DateTimeField(auto_now=True)
    is_public = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Section(models.Model):
    class Meta:
        ordering = ['section_number']

    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='section', default=None)
    name = models.CharField(max_length=128)
    section_number = models.IntegerField(default=1)
    all_the_students_can_see = models.BooleanField(default=True)
    can_see = models.ManyToManyField(User, related_name='section_can_see', blank=True)

    def __str__(self):
        return self.name
    

class Lesson(models.Model):
    class Meta:
        ordering = ['lesson_number']

    section = models.ForeignKey(Section, on_delete=models.CASCADE, related_name='lesson', default=None)
    name = models.CharField(max_length=128)
    lesson_number = models.IntegerField(default=1)
    description = models.CharField(max_length=256, blank=True)
    lecturer = models.CharField(max_length=256)
    video = models.FileField(upload_to=lesson_directory_path,validators=[FileExtensionValidator(['mp4', 'avi', ])], null=True)  
    all_the_students_can_see = models.BooleanField(default=True)
    can_see = models.ManyToManyField(User, related_name='lesson_can_see', blank=True)
    
    def __str__(self):
        return self.name